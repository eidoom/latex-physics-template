\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{physics-article}[2019/05/08 Ryan's custom physics article class]

\LoadClass[10pt, a4paper]{article}

\usepackage{mathtools,amssymb,slashed,physics,graphicx}
\usepackage[colorlinks,linkcolor=blue,urlcolor=blue,citecolor=blue]{hyperref}
\usepackage[capitalize]{cleveref}
\usepackage[compat=1.1.0]{tikz-feynman}
\usepackage{letltxmacro}

\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
	\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
	\advance\dimen0-0.2\ht0
	\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
	{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2}}
\makeatother

\makeatletter
\g@addto@macro\bfseries{\boldmath}
\makeatother

\crefname{section}{Sec.}{Secs.}
\Crefname{section}{Section}{Sections}
\crefname{appendix}{App.}{Apps.}

\newcommand{\ie}{\textit{i.e.}~}
\newcommand{\cf}{\textit{c.f.}~}

\newcommand{\alphaqed}{{\alpha}}
\newcommand{\aem}{\alphaqed}
\newcommand{\alphaqcd}{{\alpha_\mathrm{s}}}
\newcommand{\astr}{\alphaqcd}
\newcommand{\couplingqcd}{{\mathrm{g}_\mathrm{s}}}
\newcommand{\cs}{\couplingqcd}
\newcommand{\couplingqed}{{\mathrm{e}}}
\newcommand{\cem}{\couplingqed}
\newcommand{\fractionalcharge}{{\mathrm{Q}_\mathrm{q}}}
\newcommand{\fc}{\fractionalcharge}
\newcommand{\amplitude}{\mathcal{A}}
\newcommand{\am}{\amplitude}
\newcommand{\amplitudesquared}{\abs{\mathcal{A}}^2}
\newcommand{\as}{\amplitudesquared}
\newcommand{\ant}{\amplitude_n^\mathrm{tree}}
\newcommand{\partialamplitude}{A}
\newcommand{\pa}{\partialamplitude}
\newcommand{\partialamplitudetree}[2]{\partialamplitude_{#2}^{\mathrm{tree}}\pqty{#1}}
\newcommand{\partialamplitudentree}[1]{\partialamplitudetree{#1}{n}}
\newcommand{\pat}{\partialamplitudetree}
\newcommand{\pant}{\partialamplitudentree}
\newcommand{\realamplitude}{\amplitude_{\mathrm{R}}}
\newcommand{\ra}{\realamplitude}
\newcommand{\virtualamplitude}{\amplitude_{\mathrm{V}}}
\newcommand{\numbercolours}{{\mathrm{N}_\mathrm{c}}}
\newcommand{\nc}{\numbercolours}
\newcommand{\U}[1]{\mathrm{U}\pqty{#1}}
\newcommand{\SU}[1]{\mathrm{S}\U{#1}}
\newcommand{\SUt}[1]{$\SU{#1}$}
\newcommand{\colourgeneratorfundamental}{t}
\newcommand{\cgf}{\colourgeneratorfundamental}
\newcommand{\colourgeneratoradjoint}{F}
\newcommand{\cga}{\colourgeneratoradjoint}
\newcommand{\colourdynkinindex}{{\mathrm{T}_\mathrm{F}}}
\newcommand{\cdi}{\colourdynkinindex}
\newcommand{\casimir}{\mathrm{C}}
\newcommand{\casimirfundamental}{{\casimir_\mathrm{F}}}
\newcommand{\caf}{\casimirfundamental}
\newcommand{\casimiradjoint}{{\casimir_\mathrm{A}}}
\newcommand{\caa}{\casimiradjoint}
\newcommand{\spindimension}{{d_s}}
\newcommand{\cm}{\colourmatrix}
\newcommand{\colourfactor}{c}
\newcommand{\cof}{\colourfactor}
\newcommand{\crosssection}{\sigma}
\newcommand{\realcrosssection}{\crosssection_{\mathrm{R}}}
\newcommand{\rcs}{\realcrosssection}
\newcommand{\virtualcrosssection}{\crosssection_{\mathrm{V}}}
\newcommand{\vcs}{\virtualcrosssection}
\newcommand{\sigmaLO}{\crosssection_0}
\newcommand{\sigmaNLO}{\crosssection^{\mathrm{(NLO)}}}
\newcommand{\colourmatrix}{\mathcal{C}}
\newcommand{\globalcounterterm}{\mathcal{I}}
\newcommand{\gct}{\globalcounterterm}
\newcommand{\localcounterterm}{\dd{\mathcal{D}}}
\newcommand{\lct}{\localcounterterm}
\newcommand{\genericoperator}{\mathcal{O}}
\newcommand{\go}{\genericoperator}
\newcommand{\collinearphasespacefourfive}{\dd{\Phi_{4\parallel5}}}
\newcommand{\cps}{\collinearphasespacefourfive}
\newcommand{\explicitphasespace}[3]{#1\pqty{#2 \rightarrow #3}}
\newcommand{\bornphasespacefactorised}{\dd{{\Phi}_2}}
\newcommand{\bpsf}{\bornphasespacefactorised}
\newcommand{\pcps}[2]{\explicitphasespace{\collinearphasespacefourfive}{#1}{#2}}
\newcommand{\pbpsf}[2]{\explicitphasespace{\bornphasespacefactorised}{#1}{#2}}
\newcommand{\fluxfactor}{\mathrm{F}}
\newcommand{\ff}{\fluxfactor}
\newcommand{\structureconstant}{f}
\newcommand{\stc}{\structureconstant}
\newcommand{\covcon}[3]{{#1}_{#2}^{\phantom{#2}#3}}
\newcommand{\concov}[3]{{#1}^{#2}_{\phantom{#2}#3}}
\newcommand{\rightweylspinor}{\lambda}
\newcommand{\rws}{\rightweylspinor}
\newcommand{\leftweylspinor}{\tilde{\rws}}
\newcommand{\lws}{\leftweylspinor}
\newcommand{\lefthandedweylspinorup}[2]{\covcon{\lws}{#1}{\dot{#2}}}
\newcommand{\lwsu}{\lefthandedweylspinorup}
\newcommand{\righthandedweylspinorup}[2]{\covcon{\rws}{#1}{#2}}
\newcommand{\rwsu}{\righthandedweylspinorup}
\newcommand{\lefthandedweylspinordown}[2]{{(\lws_{#1}})_{\dot{#2}}}
\newcommand{\lwsd}{\lefthandedweylspinordown}
\newcommand{\righthandedweylspinordown}[2]{(\rws_{#1})_{#2}}
\newcommand{\rwsd}{\righthandedweylspinordown}
\newcommand{\levicivita}{\varepsilon}
\newcommand{\lc}{\levicivita}
\newcommand{\llc}[3]{\lc#1{\dot{#2}\dot{#3}}}
\newcommand{\llcu}[2]{\llc{^}{#1}{#2}}
\newcommand{\llcd}[2]{\llc{_}{#1}{#2}}
\newcommand{\rlc}[3]{\lc#1{#2#3}}
\newcommand{\rlcu}[2]{\rlc{^}{#1}{#2}}
\newcommand{\rlcd}[2]{\rlc{_}{#1}{#2}}
\newcommand{\leftinnerproduct}[1]{\bqty*{ #1 }}
\newcommand{\lip}{\leftinnerproduct}
\newcommand{\rightinnerproduct}[1]{\left\langle #1 \right\rangle}
\newcommand{\rip}{\rightinnerproduct}
\newcommand{\ptr}[1]{\tr\pqty{#1}}
\newcommand{\pdet}[1]{\det\pqty{#1}}
\newcommand{\imi}{{\mathrm{i}\mkern1mu}}
\newcommand{\s}[1]{{\,\text{s}_{#1}}}
\newcommand{\sh}[1]{{\,\hat{\text{s}}_{#1}}}
\newcommand{\sumsquare}[1]{\overline{\abs{#1}^2}}
\newcommand{\deltaplus}[1]{\delta^{(+)}\qty(#1)}
\newcommand{\nth}[1]{{#1}^{\mathrm{th}}}
\newcommand{\polarisationvector}{\varepsilon}
\newcommand{\pov}{\polarisationvector}
\newcommand{\complexnumbers}{\mathbb{C}}
\newcommand{\cn}{\complexnumbers}
\newcommand{\collinearlimit}[2]{$#1\!\parallel\!#2$}
\newcommand{\cl}{\collinearlimit}

\newcommand{\cutmargin}{1ex}
\newcommand{\cut}[2]{\draw [dashed] ($(#1) + (0, \cutmargin)$) -- ($(#2) + (0, -\cutmargin)$);}

\newif\ifrenderdiagrams
% Set true or false here:
\renderdiagramstrue
% \renderdiagramsfalse

\newcommand{\newdiagram}[2]{
	\ifrenderdiagrams
	\newcommand{#1}{#2}
	\else
	\newcommand{#1}{\begin{center}[Diagram]\\\end{center}}
	\fi
}

\renewcommand{\abstractname}{\vspace{-\baselineskip}}
