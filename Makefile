DOCNAME=article
TEX=lualatex
BIB=bibtex

all: report view

.PHONY: clean

report:
	$(TEX) $(DOCNAME).tex
	$(BIB) $(DOCNAME).aux
	$(TEX) $(DOCNAME).tex
	$(TEX) $(DOCNAME).tex

view: 
	evince $(DOCNAME).pdf &

clean:
	rm -f *.blg *.bbl *.aux *.log *.out *.toc *.synctex.gz

